# Extensions

Midori has partial support for Chrome extensions (see (https://github.com/grupoastian/midori-desktop/issues)).

# Installing an extension

To install an extension, you will need to extract the `crx` file of the extension and put the extracted folder to `extensions` directory.

The `extensions` directory paths:
- On Linux and macOS: `~/.midori/extensions`
- On Windows: `%USERPROFILE%/.midori/extensions`
